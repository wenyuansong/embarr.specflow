﻿using Embarr.SpecFlow.AcceptanceTests.Models;
using Embarr.SpecFlow.Extensions;
using Embarr.SpecFlow.Services;
using Should;
using TechTalk.SpecFlow;

namespace Embarr.SpecFlow.AcceptanceTests.Steps
{
    [Binding]
    public class TableExtensionsSteps
    {
        private readonly FeatureContextService featureContextService;
        private readonly ScenarioContextService scenarioContextService;

        public TableExtensionsSteps(FeatureContextService featureContextService, ScenarioContextService scenarioContextService)
        {
            this.featureContextService = featureContextService;
            this.scenarioContextService = scenarioContextService;
        }

        [When(@"the following deep properties are hydrated:")]
        public void WhenTheFollowingDeepPropertiesAreHydrated(Table table)
        {
            featureContextService.SaveValue<string>(null);
            
            var deepTable = table.CreateDeepInstance<DeepInstanceModel>();
            scenarioContextService.SaveValue(deepTable);
            ScenarioContext.Current.Add(typeof(DeepInstanceModel).Name, deepTable);
        }

        [Then(@"the following properties shoud be populated:")]
        public void ThenTheFollowingPropertiesShoudBePopulated(Table table)
        {
            var instanceUnderTest = scenarioContextService.GetValue<DeepInstanceModel>();
            
            var result = table.ValidateDeepInstance(instanceUnderTest);

            result.AllMatch.ShouldBeTrue();
        }

        [When(@"the following deep properties are hydrated and filled into a model:")]
        public void WhenTheFollowingDeepPropertiesAreHydratedAndFillIntoAModel(Table table)
        {
            featureContextService.SaveValue<string>(null);

            var deepTable = new DeepInstanceModel();
            table.FillDeepInstance(deepTable);
            scenarioContextService.SaveValue(deepTable);
            ScenarioContext.Current.Add(typeof(DeepInstanceModel).Name, deepTable);
        }

    }
}
