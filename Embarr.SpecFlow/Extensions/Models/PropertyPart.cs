﻿namespace Embarr.SpecFlow.Extensions.Models
{
    internal class PropertyPart
    {
        public string Name { get; set; }

        public string Path { get; set; }

        public string Key { get; set; }

        public PropertyType Type
        {
            get { return Key == null ? PropertyType.Standard : PropertyType.KeyedOrIndexer; }
        }
    }
}