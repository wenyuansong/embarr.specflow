﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Embarr.SpecFlow.Extensions.Models;

namespace Embarr.SpecFlow.Extensions.Services
{
    internal static class PropertyInstanceService
    {
        public static void BuildProperty(Property property, int partIndex, object parent)
        {
            var propertyInfo = parent.GetType().GetProperty(property.Parts[partIndex].Name);

            if (!propertyInfo.CanWrite)
            {
                return;
            }

            if (IsSimpleType(propertyInfo))
            {
                var simpleTypeValue = GetSimpleTypeValue(property.RawStringValue, propertyInfo.PropertyType);
                propertyInfo.SetValue(parent, simpleTypeValue);
                return;
            }

            if (property.Parts[partIndex].Type == PropertyType.Standard)
            {
                var childInstance = propertyInfo.GetValue(parent);
                if (childInstance == null)
                {
                    childInstance = Activator.CreateInstance(propertyInfo.PropertyType);
                    propertyInfo.SetValue(parent, childInstance);
                }

                BuildProperty(property, partIndex + 1, childInstance);
                return;
            }

            if (propertyInfo.PropertyType.IsArray)
            {
                var propertyElementType = propertyInfo.PropertyType.GetElementType();

                var childInstance = (IList)propertyInfo.GetValue(parent);

                if (childInstance == null)
                {
                    childInstance = Array.CreateInstance(propertyElementType, 0);
                    propertyInfo.SetValue(parent, childInstance);
                }

                if (IsSimpleType(propertyElementType))
                {
                    Array newArray = Array.CreateInstance(propertyElementType, childInstance.Count + 1);
                    Array.Copy((Array)childInstance, newArray, childInstance.Count);
                    childInstance = newArray;
                    childInstance[childInstance.Count - 1] = GetSimpleTypeValue(property.RawStringValue, propertyElementType);
                    propertyInfo.SetValue(parent, childInstance);
                }
                else
                {
                    var indexPosition = int.Parse(property.Parts[partIndex].Key);
                    object item;
                    if (childInstance.Count > indexPosition)
                    {
                        item = childInstance[indexPosition];
                    }
                    else
                    {
                        item = Activator.CreateInstance(propertyElementType);

                        Array newArray = Array.CreateInstance(propertyElementType, childInstance.Count + 1);
                        Array.Copy((Array)childInstance, newArray, childInstance.Count);
                        childInstance = newArray;
                        childInstance[childInstance.Count - 1] = item;
                    }

                    propertyInfo.SetValue(parent, childInstance);
                    BuildProperty(property, partIndex + 1, item);
                }

                return;
            }

            if (propertyInfo.PropertyType.FindInterfaces(MyInterfaceFilter, typeof(IList<>).FullName).Length > 0)
            {
                var childInstance = propertyInfo.GetValue(parent);
                var genericTypeArgument = propertyInfo.PropertyType.GenericTypeArguments[0];
                if (childInstance == null)
                {
                    var listType = typeof(List<>);
                    var constructedListType = listType.MakeGenericType(genericTypeArgument);
                    childInstance = Activator.CreateInstance(constructedListType);
                    propertyInfo.SetValue(parent, childInstance);
                }

                if (IsSimpleType(genericTypeArgument))
                {
                    ((IList)childInstance).Add(GetSimpleTypeValue(property.RawStringValue, genericTypeArgument));
                }
                else
                {
                    var indexPosition = int.Parse(property.Parts[partIndex].Key);
                    object item;
                    if (((IList)childInstance).Count > indexPosition)
                    {
                        item = ((IList)childInstance)[indexPosition];
                    }
                    else
                    {
                        item = Activator.CreateInstance(genericTypeArgument);
                        ((IList)childInstance).Add(item);
                    }

                    BuildProperty(property, partIndex + 1, item);
                }

                return;
            }

            if (propertyInfo.PropertyType.FindInterfaces(MyInterfaceFilter, typeof(IDictionary<,>).FullName).Length > 0)
            {
                var childInstance = propertyInfo.GetValue(parent);
                var genericTypeKeyArgument = propertyInfo.PropertyType.GenericTypeArguments[0];
                var genericTypeValueArgument = propertyInfo.PropertyType.GenericTypeArguments[1];
                if (childInstance == null)
                {
                    var dictionaryType = typeof(Dictionary<,>);
                    var constructedListType = dictionaryType.MakeGenericType(genericTypeKeyArgument, genericTypeValueArgument);
                    childInstance = Activator.CreateInstance(constructedListType);
                    propertyInfo.SetValue(parent, childInstance);
                }

                if (IsSimpleType(genericTypeValueArgument))
                {
                    ((IDictionary)childInstance).Add(
                        GetSimpleTypeValue(property.Parts[partIndex].Key, genericTypeKeyArgument),
                        GetSimpleTypeValue(property.RawStringValue, genericTypeValueArgument));
                }
                else
                {
                    var key = int.Parse(property.Parts[partIndex].Key);
                    object item;
                    if (((IDictionary)childInstance).Count > key)
                    {
                        item = ((IDictionary)childInstance)[key];
                    }
                    else
                    {
                        item = Activator.CreateInstance(genericTypeValueArgument);
                        ((IDictionary)childInstance).Add(
                            GetSimpleTypeValue(property.Parts[partIndex].Key, genericTypeKeyArgument),
                            item);
                    }

                    BuildProperty(property, partIndex + 1, item);
                }
            }
        }

        internal static object GetInstancePropertyValue(Property property, int partIndex, object parent)
        {
            var propertyPart = property.Parts[partIndex];
            var propertyInfo = parent.GetType().GetProperty(propertyPart.Name);

            var propertyValue = propertyInfo.GetValue(parent);

            if (partIndex == property.Parts.Count - 1 && propertyPart.Type == PropertyType.Standard)
            {
                return propertyValue;
            }

            if (partIndex == property.Parts.Count - 1 && propertyPart.Type == PropertyType.KeyedOrIndexer)
            {
                if (propertyInfo.PropertyType.FindInterfaces(MyInterfaceFilter, typeof(IList<>).FullName).Length > 0)
                {
                    return ((IList)propertyValue)[int.Parse(propertyPart.Key)];
                }

                if (propertyInfo.PropertyType.FindInterfaces(MyInterfaceFilter, typeof(IDictionary<,>).FullName).Length > 0)
                {
                    return ((IDictionary)propertyValue)[propertyPart.Key];
                }
            }
            else if (partIndex < property.Parts.Count - 1 && propertyPart.Type == PropertyType.KeyedOrIndexer)
            {
                if (propertyInfo.PropertyType.FindInterfaces(MyInterfaceFilter, typeof(IList<>).FullName).Length > 0)
                {
                    return GetInstancePropertyValue(property, partIndex + 1, ((IList)propertyValue)[int.Parse(propertyPart.Key)]);
                }

                if (propertyInfo.PropertyType.FindInterfaces(MyInterfaceFilter, typeof(IDictionary<,>).FullName).Length > 0)
                {
                    return GetInstancePropertyValue(property, partIndex + 1, ((IDictionary)propertyValue)[propertyPart.Key]);
                }
            }

            return GetInstancePropertyValue(property, partIndex + 1, propertyValue);
        }

        private static bool MyInterfaceFilter(Type typeObj, Object criteriaObj)
        {
            if (typeObj.ToString().Contains(criteriaObj.ToString()))
            {
                return true;
            }

            return false;
        }

        private static bool IsSimpleType(PropertyInfo property)
        {
            if (property == null)
            {
                return false;
            }

            return property.PropertyType.IsPrimitive ||
                   property.PropertyType.IsValueType ||
                   property.PropertyType == typeof(string) ||
                   property.PropertyType.FullName.Contains("Nullable");
        }

        private static bool IsSimpleType(Type type)
        {
            if (type == null)
            {
                return false;
            }

            return type.IsPrimitive ||
                   type.IsValueType ||
                   type == typeof(string) ||
                   type.FullName.Contains("Nullable");
        }

        private static object GetSimpleTypeValue(string value, Type type)
        {
            if (type == typeof(decimal) || type == typeof(decimal?))
            {
                return decimal.Parse(value);
            }

            if (type == typeof(double) || type == typeof(double?))
            {
                return double.Parse(value);
            }

            if (type == typeof(float) || type == typeof(float?))
            {
                return float.Parse(value);
            }

            if (type == typeof(int) || type == typeof(int?))
            {
                return int.Parse(value);
            }

            if (type == typeof(Int64) || type == typeof(Int64?))
            {
                return Int64.Parse(value);
            }

            if (type == typeof(UInt64) || type == typeof(UInt64?))
            {
                return UInt64.Parse(value);
            }

            if (type == typeof(Int32) || type == typeof(Int32?))
            {
                return Int32.Parse(value);
            }

            if (type == typeof(UInt32) || type == typeof(UInt32?))
            {
                return UInt32.Parse(value);
            }

            if (type == typeof(Int16) || type == typeof(Int16?))
            {
                return Int16.Parse(value);
            }

            if (type == typeof(UInt16) || type == typeof(UInt16?))
            {
                return UInt16.Parse(value);
            }

            if (type == typeof(short) || type == typeof(short?))
            {
                return short.Parse(value);
            }

            if (type == typeof(Byte) || type == typeof(Byte?))
            {
                return Byte.Parse(value);
            }

            if (type == typeof(SByte) || type == typeof(SByte?))
            {
                return SByte.Parse(value);
            }

            if (type == typeof(bool) || type == typeof(bool?))
            {
                return int.Parse(value);
            }

            if (type == typeof(DateTime) || type == typeof(DateTime?))
            {
                return DateTime.Parse(value);
            }

            if (type == typeof(char) || type == typeof(char?))
            {
                return char.Parse(value);
            }

            return value;
        }

    }
}
