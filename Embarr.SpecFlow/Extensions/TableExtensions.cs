﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Embarr.SpecFlow.Extensions.Models;
using Embarr.SpecFlow.Extensions.Services;
using TechTalk.SpecFlow;

namespace Embarr.SpecFlow.Extensions
{
    /// <summary>
    /// Extensions for working with <see cref="Table"/>.
    /// </summary>
    public static class TableExtensions
    {
        /// <summary>
        /// Creates an instance of a model with a deep object graph when required.
        /// </summary>
        /// <typeparam name="T">The type of the model.</typeparam>
        /// <param name="table">The table.</param>
        public static T CreateDeepInstance<T>(this Table table)
        {
            var rows = table.Rows;

            if (rows.Count == 0)
            {
                return default(T);
            }

            var instance = Activator.CreateInstance<T>();

            var properties = GetProperties<T>(rows);

            foreach (var property in properties)
            {
                PropertyInstanceService.BuildProperty(property, 0, instance);
            }

            return instance;
        }

        /// <summary>
        /// Validates a deep instance.
        /// </summary>
        /// <typeparam name="T">The type of the model.</typeparam>
        /// <param name="table">The table.</param>
        /// <param name="instance">The instance to validate.</param>
        /// <returns><see cref="ValidateDeepInstanceResult"/></returns>
        public static ValidateDeepInstanceResult ValidateDeepInstance<T>(this Table table, T instance)
        {
            var rows = table.Rows;

            if (rows.Count == 0)
            {
                return null;
            }

            var validateDeepInstanceResult = new ValidateDeepInstanceResult();

            var properties = GetProperties<T>(rows);

            foreach (var property in properties)
            {
                var value = PropertyInstanceService.GetInstancePropertyValue(property, 0, instance);

                validateDeepInstanceResult.Properties.Add(new ValidateDeepInstanceProperty
                {
                    Path = property.PropertyPath,
                    ActualValue = value.ToString(),
                    ExpectedValue = property.RawStringValue
                });
            }

            validateDeepInstanceResult.SuccessfulMatches.AddRange(validateDeepInstanceResult.Properties.Where(x => x.IsMatch));
            validateDeepInstanceResult.FailedMatches.AddRange(validateDeepInstanceResult.Properties.Where(x => !x.IsMatch));

            return validateDeepInstanceResult;
        }

        /// <summary>
        /// Fill an instance of a model with a deep object graph when required.
        /// </summary>
        /// <typeparam name="T">The type of the model.</typeparam>
        /// <param name="table">The table.</param>
        /// <param name="instance">The model instance to be filled.</param>
        public static void FillDeepInstance(this Table table, object instance)
        {
            var rows = table.Rows;

            if (rows.Count == 0) return;

            var properties = GetProperties<object>(rows);

            foreach (var property in properties)
            {
                PropertyInstanceService.BuildProperty(property, 0, instance);
            }
        }

        private static List<Property> GetProperties<T>(TableRows rows)
        {
            var properties = new List<Property>();

            foreach (var row in rows)
            {
                var propertyParts = GetPropertyParts(row.Values.First());
                var value = row.Values.Last();
                properties.Add(new Property
                {
                    PropertyPath = row.Values.First(),
                    Parts = propertyParts,
                    RawStringValue = value
                });
            }
            return properties;
        }

        private static List<PropertyPart> GetPropertyParts(string propertyPath)
        {
            var parts = new List<PropertyPart>();

            var stringBuilder = new StringBuilder();

            foreach (var character in propertyPath)
            {
                if (character == '.')
                {
                    var property = new PropertyPart { Path = stringBuilder.ToString() };
                    UpdateProperty(property);
                    parts.Add(property);
                    stringBuilder.Clear();
                }
                else
                {
                    stringBuilder.Append(character);
                }
            }

            var prop = new PropertyPart { Path = stringBuilder.ToString() };
            UpdateProperty(prop);
            parts.Add(prop);
            return parts;
        }

        private static void UpdateProperty(PropertyPart propertyPart)
        {
            if (propertyPart.Path.Contains("["))
            {
                var collect = false;
                var sb = new StringBuilder();
                for (var i = 0; i < propertyPart.Path.Length; i++)
                {
                    if (propertyPart.Path[i] == ']')
                    {
                        collect = false;
                    }

                    if (collect)
                    {
                        sb.Append(propertyPart.Path[i]);
                    }

                    if (propertyPart.Path[i] == '[')
                    {
                        propertyPart.Name = propertyPart.Path.Substring(0, i);
                        collect = true;
                    }
                }

                var result = sb.ToString();

                propertyPart.Key = result.Replace("\"", string.Empty);
            }
            else
            {
                propertyPart.Name = propertyPart.Path;
            }
        }
    }
}
