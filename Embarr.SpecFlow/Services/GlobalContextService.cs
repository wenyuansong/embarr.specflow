using System.Collections.Generic;

namespace Embarr.SpecFlow.Services
{
    /// <summary>
    /// Service for accessing a global test context
    /// </summary>
    public class GlobalContextService : ContextService
    {
        private static readonly Dictionary<string, object> GlobalContextItems = new Dictionary<string, object>();

        /// <summary>
        /// Initializes a new instance of the <see cref="GlobalContextService"/> class.
        /// </summary>
        public GlobalContextService()
            : base(GlobalContextItems)
        {
            
        }
    }
}